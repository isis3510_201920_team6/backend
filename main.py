import firebase_admin
from firebase_admin import credentials, firestore
import random
import time

cred = credentials.Certificate("./t-app-31605-firebase-adminsdk-agbrb-cd6a608a55.json")
firebase_admin.initialize_app(cred)
db = firestore.client()

tripsRef = db.collection(u'trips')
passengersRef = db.collection(u'passengers')

locations = open("C:/Users/Diego/Google Drive/2019-2/Móviles/sprint 2/database/locations2.csv", "r+")
locations.readline()

locs = []

for line in locations:
    loc = line.rstrip().split(",")
    loc = {"id": loc[0], "lat":float(loc[2]), "lon":float(loc[3])}
    locs.append(loc)

locations.close()

locs = sorted(locs, key=lambda i: i['lat'])

stops = []

for i in range(50):
    l = random.randint(5, 15)
    s = []
    r = random.randint(0, len(locs)-15)
    for j in range(l):
        loc = locs[r+j]
        s.append({"stop_seq_number": j, "lat":loc["lat"], "lon": loc["lon"]})
    stops.append(s)


passengersCollection = passengersRef.stream()

def str_time_prop(start, end, format, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))

    ptime = stime + prop * (etime - stime)

    return ptime


def random_date(start, end, prop):
    return str_time_prop(start, end, '%m/%d/%Y %I:%M %p', prop)

startEndTimes = []

for stop in range(len(stops)): 
    startTime = (random_date("09/1/2019 1:30 PM", "10/1/2019 1:30 PM", random.random()))
    endTime = startTime + random.randrange(1800, 7200)
    startEndTimes.append({'start_time': int(startTime), 'end_time': int(endTime)})


driversRef = db.collection(u'drivers')

driver = db.document('drivers/1ur36ocYDgMbyrxjMIHf')

busesCollection = db.collection(u'buses').stream()

buses = []

for busDoc in busesCollection:
    buses.append(busDoc)

passengers = []

for passenger in passengersCollection:
    passengers.append(passenger.reference)

for i in range(len(stops)):
    bus = buses[random.randint(0, len(buses)-1)]
    busDict = bus.to_dict()
    nPassengers = random.randint(5, busDict['capacity'])
    times = startEndTimes.pop()
    tripPassengers = []
    stopsCopy = stops[i]
    for j in range(nPassengers):
        r1 = random.randint(0, len(stopsCopy)//2)
        r2 = random.randint(r1+1, len(stopsCopy)-1)
        pick_up_stop = stopsCopy[r1]
        drop_off_stop = stopsCopy[r2]
        tripPassengers.append({"passenger_id": passengers[random.randint(0, len(passengers)-1)], "pick_up_stop": pick_up_stop, "drop_off_stop": drop_off_stop})
    
    trip = {'start_time': times['start_time'], 'end_time': times['end_time'], 'driver': driver, 'bus': bus.reference, 'stops': stops[i], 'passengers': tripPassengers}
    db.collection(u'trips').add(trip)

print('done')