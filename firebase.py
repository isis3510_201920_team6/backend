import firebase_admin
from firebase_admin import credentials, firestore
import xlwt
from xlwt import Workbook


cred = credentials.Certificate("./t-app-31605-firebase-adminsdk-agbrb-1ddadfb79f.json")
firebase_admin.initialize_app(cred)
db = firestore.client()


tripsRef = db.collection(u'trips').stream()
tripsRef2= db.collection(u'trips').stream()
tripsRef3= db.collection(u'trips').stream()
wb = Workbook()


def getDropOffStops():
    passengersDropOffList = []
    dropOffSheet = wb.add_sheet('Drop Off Stops')
    dropOffSheet.write(1, 0, 'Passenger drop off Lon')
    dropOffSheet.write(1, 1, 'Passenger drop off Lat')
    for trip in tripsRef:
        reftrip = trip.to_dict()
        passengersDropOff=reftrip["passengers"]
        passengersSize = len(passengersDropOff)
        for i in range(0,passengersSize):
            lon=passengersDropOff[i]["drop_off_stop"]["lon"]
            lat=passengersDropOff[i]["drop_off_stop"]["lat"]
            passengersDropOffList.append({'lat': lat, 'lon': lon})
            #passengersLonList.append(passengers[i]["drop_off_stop"]["lon"])
    x = 1
    while x < len(passengersDropOffList):
        dropOffSheet.write(x + 1, 0, passengersDropOffList[x]["lon"])
        dropOffSheet.write(x + 1, 1, passengersDropOffList[x]["lat"])
        x = x + 1
    wb.save('FirebaseFetch.xls')

def getPickUpStops():
    passengersPickUpList = []
    pickUpSheet = wb.add_sheet('Pick Up Stops')
    pickUpSheet.write(1, 0, 'Passenger pick up Lon')
    pickUpSheet.write(1, 1, 'Passenger pick up Lat')
    for trip in tripsRef2:
        reftrip2 = trip.to_dict()
        passengersPickUp=reftrip2["passengers"]
        passengersSize = len(passengersPickUp)
        for i in range(0,passengersSize):
            lon=passengersPickUp[i]["pick_up_stop"]["lon"]
            lat=passengersPickUp[i]["pick_up_stop"]["lat"]
            passengersPickUpList.append({'lat':lat, 'lon':lon})
            #passengersPickUpList.append(passengers[i]["drop_off_stop"]["lat"])

    x = 1
    while x < len(passengersPickUpList):
        pickUpSheet.write(x + 1, 0, passengersPickUpList[x]["lon"])
        pickUpSheet.write(x + 1, 1, passengersPickUpList[x]["lat"])
        x = x + 1
    wb.save('FirebaseFetch.xls')
def getStopsTrips():
    stopsList = []
    stopListSheet = wb.add_sheet('Stops')
    stopListSheet.write(1, 0, 'Lon')
    stopListSheet.write(1, 1, 'Lat')
    for trip in tripsRef3:
        reftrip3 = trip.to_dict()
        stops = reftrip3["stops"]
        stopsSize = len(stops)
        for i in range(0, stopsSize):
            lon = stops[i]["lon"]
            lat = stops[i]["lat"]
            stopsList.append({'lat': lat, 'lon': lon})
            # passengersPickUpList.append(passengers[i]["drop_off_stop"]["lat"])

    x=1
    while x<len(stopsList):
        stopListSheet.write(x+1, 0, stopsList[x]["lat"])
        stopListSheet.write(x + 1, 1, stopsList[x]["lon"])
        x=x+1
    wb.save('FirebaseFetch.xls')

getStopsTrips()
getPickUpStops()
getDropOffStops()